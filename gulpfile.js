'use strict';

const settings = {
  favicon: {
    source: 'assets/images/favicon/*.{json,xml,ico}',
    target: 'dist/images/',
  },

  images: {
    source: 'assets/images/*',
    sourceTinyPng: 'assets/images/**/*.{png,jpg,jpeg}',
    sourceSvgGif: 'assets/images/**/*.{svg,gif}',
    target: 'dist/images'
  },

  css: {
    source: 'assets/sass/*.scss',
    target: 'dist/css/',
    filename: 'index.css',
    watch: 'assets/sass/**/*.scss',
    watchBuild: ['dist/css/*.css'],
  }
};

const { src, dest, watch, series, parallel } = require('gulp');
const sass          = require('gulp-sass');
const postcss       = require('gulp-postcss');
const autoprefixer  = require('autoprefixer');
const cleancss      = require('gulp-clean-css');
const rename        = require('gulp-rename');
const imagemin      = require('gulp-imagemin');
const tinypng       = require('gulp-tinypng-extended');
const gulpCopy      = require('gulp-copy');

/**
 * Setup for CSS postprocessor
 */
const postCssPlugins = [
    autoprefixer({
       grid: 'autoplace'
    })
];

/**
 * Task to build and minify CSS files
 */
function scssTask() {
    return src(settings.css.source, { sourcemaps: false })
        .pipe(sass())
        .pipe(postcss(postCssPlugins))
        .pipe(cleancss({level: {1: {specialComments: 0}}}))
        .pipe(rename({suffix: '.min'}))
        .pipe(dest(settings.css.target))
}

/**
 * Image optimization task
 * Using this one just for gifs & svgs
 */
function imageTask() {
    return src(settings.images.sourceSvgGif)
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            }),
        ],
        { verbose: true }))
        .pipe(dest(settings.images.target))
}

/**
 * Another image optimization task.
 * Uses tinyPng API, which comes with 500 conversions / month
 * free of charge. This gives really good results for jpg & png images
 */
function tinypngTask() {
    return src(settings.images.sourceTinyPng)
        .pipe(tinypng({
            key: 'D5cP5ylQhfKqQvCGPN2y4NbN6cgvRpNc',
            sigFile: 'assets/images/.tinypng-sigs',
            log: true
        }))
        .pipe(dest(settings.images.target))
}

/**
 * Copy other favicon files which are not png
 */
function copyFaviconTask() {
    return src(settings.favicon.source)
        .pipe(gulpCopy(settings.favicon.target, { prefix: 2}));
}


/**
 * Automatically watch changes in different locations and reload browser
 */
function watchTask() {
    watch([settings.css.watch], series(scssTask));
}

/**
 * imagesAll - optimizes all image formats
 */
exports.imagesAll = series( imageTask, tinypngTask );

exports.watch = watchTask;

/**
 * Default gulp taks
 */
exports.default = series(
  scssTask,
  imageTask,
  tinypngTask,
  copyFaviconTask
);
